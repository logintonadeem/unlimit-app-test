package com.example.test_app.ui.adapters

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.test_app.R
import com.example.test_app.ui.data.models.JokeResponse

class JokeAdapter (list : MutableList<JokeResponse?>?) : RecyclerView.Adapter<JokeAdapter.ViewHolder>() {
    val listJokes = list

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var desc: TextView
        var count: TextView
        init {
            desc = itemView.findViewById(R.id.tvDesc)
            count = itemView.findViewById(R.id.tvCountOn)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var itemVw = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ViewHolder(itemVw)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.desc.setText(listJokes?.get(position)?.joke)
            holder.count.setText("${position + 1}")
    }

    override fun getItemCount(): Int {
            return listJokes?.size!!;
    }
}