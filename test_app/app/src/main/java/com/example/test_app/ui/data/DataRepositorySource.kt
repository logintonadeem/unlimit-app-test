package com.example.test_app.ui.data

import com.example.test_app.ui.data.models.JokeResponse
import kotlinx.coroutines.flow.Flow


interface DataRepositorySource {
    suspend fun jokeList(registerInput : MutableMap<String, Any>): Flow<Resource<JokeResponse>>
}
