package com.example.test_app.ui.activities

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.activity.ComponentActivity
import androidx.activity.viewModels
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.test_app.databinding.MainActivityBinding
import com.example.test_app.ui.adapters.JokeAdapter
import com.example.test_app.ui.data.Resource
import com.example.test_app.ui.data.models.JokeResponse
import com.example.test_app.ui.db.JokesRepository
import com.example.test_app.ui.utils.observe
import com.example.test_app.ui.viewmodel.JokeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    private lateinit var binding : MainActivityBinding;
    private lateinit var repository: JokesRepository
    var jokeList : MutableList<JokeResponse?>? = mutableListOf()
    private val jokeViewModel: JokeViewModel by viewModels()

    lateinit var mainHandler: Handler

    private val updateTextTask = object : Runnable {
        override fun run() {
            callApi()
            mainHandler.postDelayed(this, (1000 * 60))
        }
    }

    override fun observeViewModel() {
        observe(jokeViewModel.retailerLiveData, ::   handleResult)
    }

    override fun initViewBinding() {
        binding = MainActivityBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)

        mainHandler = Handler(Looper.getMainLooper())
        repository = JokesRepository(application, jokeViewModel.viewModelScope)


        val llManager: LinearLayoutManager = LinearLayoutManager(this)
        binding.rvJokes.layoutManager = llManager
        jokeList = repository.allJokes
        setAdapter(jokeList)
        callApi()
    }

    private fun callApi(){
        var m = mutableMapOf<String, Any>("format" to "json")
        jokeViewModel.jokeList(m)
    }

    override fun onPause() {
        super.onPause()
        mainHandler.removeCallbacks(updateTextTask)
    }

    override fun onResume() {
        super.onResume()
        mainHandler.post(updateTextTask)
    }

    override fun onDestroy() {
        super.onDestroy()
        mainHandler.removeCallbacks(updateTextTask)
    }

    private fun setAdapter(list : MutableList<JokeResponse?>?){
        if(list != null && list.size > 0)
            binding.rvJokes.adapter = JokeAdapter(list)
    }

    private fun handleResult(status: Resource<JokeResponse>) {

        when (status) {
            is Resource.Loading -> binding.rvJokes.visibility = View.VISIBLE
            is Resource.Success -> status.data?.let {
                binding.rvJokes.visibility = View.VISIBLE
                repository.insert(it)
                repository.getAllJokes()
                jokeList = repository.allJokes
                setAdapter(jokeList)
            }
            is Resource.DataError -> {

            }
        }
    }


}
