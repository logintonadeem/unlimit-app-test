package com.example.test_app.ui.data

import com.example.test_app.ui.data.models.JokeResponse
import com.example.test_app.ui.remote.RemoteData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class DataRepository @Inject constructor(private val remoteRepository: RemoteData, private val ioDispatcher: CoroutineContext) : DataRepositorySource {

    override suspend fun jokeList(apiInput: MutableMap<String, Any>): Flow<Resource<JokeResponse>> {
        return flow {
            emit(remoteRepository.jokeList(apiInput))
        }.flowOn(ioDispatcher)
    }
}
