package com.example.test_app.ui.remote

import com.example.test_app.ui.data.Resource
import com.example.test_app.ui.data.models.JokeResponse


internal interface RemoteDataSource {

    suspend fun jokeList(registerInput: MutableMap<String, Any>): Resource<JokeResponse>
}
