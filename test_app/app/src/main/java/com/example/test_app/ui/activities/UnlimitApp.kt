package com.example.test_app.ui.activities

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
open class UnlimitApp : Application(){

    override fun onCreate() {
        super.onCreate()

    }
}