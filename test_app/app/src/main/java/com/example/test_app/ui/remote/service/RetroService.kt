package com.example.test_app.ui.remote.service

import android.util.Log
import com.example.test_app.ui.data.models.JokeResponse
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Response
import retrofit2.http.*
import java.io.File


interface RetroService {

    @GET("api?format=json")
    suspend fun  jokeList(): Response<JokeResponse>

}
