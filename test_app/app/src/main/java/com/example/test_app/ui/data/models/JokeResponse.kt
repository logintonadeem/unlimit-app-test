package com.example.test_app.ui.data.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "tbl_jokes")
data class JokeResponse(
	@PrimaryKey(autoGenerate = true) @Json(name="id")
	var uid: Int = 0,
	@ColumnInfo @Json(name="joke")
	val joke: String? = null,
	@ColumnInfo@Json(name="datetime")
	val datetime: Long? = System.currentTimeMillis()
) : Parcelable