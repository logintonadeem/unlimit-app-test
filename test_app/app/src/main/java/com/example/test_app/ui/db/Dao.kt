package com.example.test_app.ui.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.test_app.ui.data.models.JokeResponse


@Dao
interface Dao {
    // below method is use to
    // add data to database.
    @Insert
    suspend fun insert(model: JokeResponse?)

    // below method is use to update
    // the data in our database.
    @Update
    suspend fun update(model: JokeResponse?)

    // below line is use to delete a
    // specific course in our database.
    @Delete
    suspend fun delete(model: JokeResponse?)

    // on below line we are making query to
    // delete all courses from our database.
    @Query("DELETE FROM tbl_jokes")
    suspend fun deleteAllCourses()

    // below line is to read all the courses from our database.
    // in this we are ordering our courses in ascending order
    // with our course name.
    @Query("SELECT * FROM tbl_jokes ORDER BY datetime DESC LIMIT 10")
    suspend fun allJokes() : MutableList<JokeResponse?>?
}