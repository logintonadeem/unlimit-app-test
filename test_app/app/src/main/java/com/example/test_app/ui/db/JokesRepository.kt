package com.example.test_app.ui.db

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.test_app.ui.data.models.JokeResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


class JokesRepository(application: Application?, scope: CoroutineScope) {
    // below line is the create a variable
    // for dao and list for all courses.
    private val dao: Dao
    private val scope : CoroutineScope

    // below method is to read all the courses.
    var allJokes: MutableList<JokeResponse?>? = null

    // creating a constructor for our variables
    // and passing the variables to it.
    init {
        val database = JokesDatabase.getInstance(application!!)
        dao = database!!.Dao()
        this.scope = scope
        getAllJokes()
    }

    // creating a method to insert the data to our database.
     fun insert(model: JokeResponse?) {
        scope.launch {
            dao.insert(model)
        }
    }

    // creating a method to getall the data from our database.
     fun getAllJokes() {
        scope.launch {
            allJokes = dao.allJokes()
        }
    }

    // creating a method to update data in database.
    fun update(model: JokeResponse?) {
        scope.launch {
            dao.update(model)
        }
    }

    // creating a method to delete the data in our database.
    fun delete(model: JokeResponse?) {
        scope.launch {
            dao.delete(model)
        }
    }

    // below is the method to delete all the courses.
    fun deleteAll() {
        scope.launch {
            dao.deleteAllCourses()
        }
    }
}