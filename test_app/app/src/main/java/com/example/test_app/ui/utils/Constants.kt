package com.example.test_app.ui.utils

const val BASE_URL = "https://geek-jokes.sameerkumar.website/"
const val NO_INTERNET_CONNECTION = -1
const val NETWORK_ERROR = -2