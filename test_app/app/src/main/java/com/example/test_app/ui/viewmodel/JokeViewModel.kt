package com.example.test_app.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.test_app.ui.data.DataRepositorySource
import com.example.test_app.ui.data.Resource
import com.example.test_app.ui.data.models.JokeResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import androidx.lifecycle.viewModelScope
import com.example.test_app.ui.utils.wrapEspressoIdlingResource
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class JokeViewModel @Inject constructor(private val dataRepositoryRepository: DataRepositorySource): BaseViewModel(){
    private val jokeLiveDataPrivate = MutableLiveData<Resource<JokeResponse>>()
    val retailerLiveData: LiveData<Resource<JokeResponse>> get() = jokeLiveDataPrivate

    fun jokeList(registerInput : MutableMap<String, Any>) {

        viewModelScope.launch {
            jokeLiveDataPrivate.value = Resource.Loading()
            wrapEspressoIdlingResource {
                dataRepositoryRepository.jokeList(registerInput).collect {
                    jokeLiveDataPrivate.value = it
                }
            }
        }
    }
}