package com.example.test_app.ui.remote

import com.example.test_app.ui.data.Resource
import com.example.test_app.ui.data.models.JokeResponse
import com.example.test_app.ui.remote.service.RetroService
import com.example.test_app.ui.utils.NETWORK_ERROR
import com.example.test_app.ui.utils.NO_INTERNET_CONNECTION
import com.example.test_app.ui.utils.NetworkConnectivity
import com.google.gson.Gson
import okhttp3.MultipartBody
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

class RemoteData @Inject
constructor(private val serviceGenerator: ServiceGenerator, private val networkConnectivity: NetworkConnectivity) : RemoteDataSource {

    override suspend fun jokeList(apiInput: MutableMap<String, Any>): Resource<JokeResponse> {
        val retroSrvice = serviceGenerator.createService(RetroService::class.java)
        return when (val response = processCall { retroSrvice.jokeList() }) {

            is List<*> -> {
                Resource.Success(response as JokeResponse)
            }
            else -> {
                if((response as retrofit2.Response<*>).body() != null){
                    Resource.Success((response as retrofit2.Response<*>).body() as JokeResponse)
                }else{
                    Resource.Success(Gson().fromJson((response as retrofit2.Response<*>).errorBody()!!.charStream(), JokeResponse::class.java))
                }
            }
        }
    }

    private suspend fun processCall(responseCall: suspend () -> Response<*>): Any? {
        if (!networkConnectivity.isConnected()) {
            return NO_INTERNET_CONNECTION
        }
        return try {
            val response = responseCall.invoke()
            val responseCode = response.code()
            if (response.isSuccessful) {
//                response.body()
                response
            } else {
                response
//                Log.e("line 54", response.raw().toString())
//                Gson().fromJson(response.errorBody()!!.charStream(), EmployeeResult::class.java)
//                response.raw().toString()
            }
        } catch (e: IOException) {
            NETWORK_ERROR
        }
    }
}
